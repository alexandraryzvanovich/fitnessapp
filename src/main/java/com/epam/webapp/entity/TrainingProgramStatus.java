package com.epam.webapp.entity;

public enum TrainingProgramStatus {
    NEW, IN_PROGRESS, DONE, DECLINED

}
