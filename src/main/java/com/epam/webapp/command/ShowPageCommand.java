package com.epam.webapp.command;

import com.epam.webapp.exception.ServiceException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class ShowPageCommand implements Command {
    private String page;

    public ShowPageCommand(final String page){
        this.page = page;
    }

    @Override
    public CommandResult execute(HttpServletRequest request) throws ServiceException {
        return CommandResult.forward(page);
    }
}
